// Bootstrap
import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard() {
	return(
		<Row>
			<Col>
				<Card className="cardCourseCard p-3">
					<Card.Body>
						<Card.Title><h4>Sample Course</h4></Card.Title>
						<Card.Subtitle><h5>Description</h5></Card.Subtitle>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur.
						</Card.Text>
						<Card.Subtitle><h5>Price</h5></Card.Subtitle>
						<Card.Text>
							PhP 40,000
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>	
			</Col>
		</Row>
	)
}