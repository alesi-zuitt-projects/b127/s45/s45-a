import {Fragment} from 'react'
import coursesData from "../data/courseData"
import CourseCard from '../components/CourseCard'

export default function Courses(){
	//check if courseData database was captured
	console.log(coursesData)
	console.log(coursesData[0])
	
	//for us to be able to display all the courses from the data file, we are going to use the map()
	// the ' map' metho dloops through the individual course objects in our array and returns a component for each course

	//multiple components created through the map method must have a unique key that will help react js identify  which / elements
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp = {course} />
			)
	})
	return(
		<Fragment>
			<h1> Courses </h1>
			{courses}
		</Fragment>

		)

}